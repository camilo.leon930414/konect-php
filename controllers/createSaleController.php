<?php
include '../config/funciones.php';
include '../config/conexion.php';

csrf();
if (isset($_POST['submit']) && !hash_equals($_SESSION['csrf'], $_POST['csrf'])) {
  die();
}
if($_POST['submit'] == 'Validar'){
    $id = $_POST['idclient'];
   
    try {
        $consultaSQL = "SELECT * FROM client WHERE idclient=$id";
        $sentencia = $conexion->prepare($consultaSQL);
        $sentencia->execute();
        $result = $sentencia->fetchAll();
        $_SESSION['idccms'] = $id;
        if(count($result)>0){
            $_SESSION['firstname'] = $result[0]['firstname'];
            $_SESSION['lastname'] = $result[0]['lastname'];
            $_SESSION['tel'] = $result[0]['tel'];
        }else{
            $_SESSION['firstname'] = '' ;
            $_SESSION['lastname'] = '' ;
            $_SESSION['tel'] = '' ;
        }
        header('Location: ../views/sales/create/crear.php');
    } catch(PDOException $error) {
        print_r($error);die;
    }
}

if($_POST['submit'] == 'Enviar'){
    $id = $_POST['idclient'];
    try {
        $consultaSQL = "SELECT * FROM client WHERE idclient=$id";
        $sentencia = $conexion->prepare($consultaSQL);
        $sentencia->execute();
        $result = $sentencia->fetchAll();
        if(count($result) == 0){
            $client = [
                "idclient"   => $_POST['idclient'],
                "firstname" => $_POST['firstname'],
                "lastname"    => $_POST['lastname'],
                "tel"     => $_POST['tel']
                ];
                $consultaSQL = "INSERT INTO client (idclient, firstname, lastname, tel)";
                $consultaSQL .= "values (:" . implode(", :", array_keys($client)) . ")";
                $sentencia = $conexion->prepare($consultaSQL);
                $sentencia->execute($client);
        }
    } catch(PDOException $error) {
        print_r($error);
    }
    try {
        //validar si hay producto
        $idproduct = $_POST['idproduct'];
        $cant = $_POST['cant'];
        $consultaSQL = "UPDATE product SET stock = (stock- $cant) WHERE id = $idproduct AND stock >=$cant";

        $sentencia = $conexion->prepare($consultaSQL);
        $sentencia->execute();
        $product = [
        "idClient"   => $_POST['idclient'],
        "idProduct" => $idproduct,
        "countSell"    => $cant,
        ];
        $consultaSQL = "INSERT INTO sales (idClient, idProduct, countSell)";
        $consultaSQL .= "values (:" . implode(", :", array_keys($product)) . ")";
        $sentencia = $conexion->prepare($consultaSQL);
        $sentencia->execute($product);
        header('Location: ../views/sales/create/crear.php');
        } catch(PDOException $error) {
            print_r($error);
        }
   
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Konecta Crud</title>
    <link rel="stylesheet" href="http://localhost/proyectos/konecta-php-nativo/library/bootstrap-5.0.2-dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="http://localhost/proyectos/konecta-php-nativo/library/css/styles.css" />
    <script src="http://localhost/proyectos/konecta-php-nativo/library/bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
    <script src="http://localhost/proyectos/konecta-php-nativo/library/jquery/jquery.js"></script>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="/proyectos/konecta-php-nativo/index.php">Konecta</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/proyectos/konecta-php-nativo/index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/proyectos/konecta-php-nativo/views/products/list/listproducts.php">Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/proyectos/konecta-php-nativo/views/sales/create/crear.php">Sales</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</body>
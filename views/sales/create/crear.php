<?php
include '../../../config/funciones.php';
include '../../../config/conexion.php';
    csrf();
    try {
        $consultaSQL = "SELECT * FROM product";
        $sentenciaSelect = $conexion->prepare($consultaSQL);
        $sentenciaSelect->execute();
        $optionsSelect = $sentenciaSelect->fetchAll();
    } catch(PDOException $error) {
        $resultado['error'] = true;
        $resultado['mensaje'] = $error->getMessage();
    }
?>
<?php include '../../../templates/header.php'; ?>

<div class="container box">
  <div class="row">
    <div class="col-md-12">
      <h2 class="mt-4">Create Sale</h2>
      <form method="post" action="../../../controllers/createSaleController.php">
        <div class="row">
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-10">
                  <div class="form-group">
                    <label for="idclient">Id Client</label>
                    <input type="number" name="idclient" id="idclient" class="form-control" value="<?php echo isset($_SESSION['idccms'])? $_SESSION['idccms']:'';?>" required>
                  </div>
                </div>
                <div class="col-md-2 mt-4">
                    <input name="csrf" type="hidden" value="<?php echo $_SESSION['csrf']; ?>">
                  <input type="submit" name="submit" class="btn btn-primary" value="Validar">
                </div>
              </div>
              <div class="form-group">
                <label for="firstname">First Name</label>
                <input type="text" name="firstname" id="firstname" class="form-control" value="<?php echo isset($_SESSION['firstname'])? $_SESSION['firstname']:'';?>" <?php echo isset($_SESSION['firstname']) && !empty($_SESSION['firstname'])? 'readonly' :'';?>>
              </div>
              <div class="form-group">
                <label for="lastname">Last Name</label>
                <input type="text" name="lastname" id="lastname" class="form-control" value="<?php echo isset($_SESSION['lastname'])? $_SESSION['lastname']:'';?>" <?php echo isset($_SESSION['lastname']) && !empty($_SESSION['lastname'])? 'readonly' :'';?>>
              </div>
              <div class="form-group">
                <label for="tel">Tel</label>
                <input type="number" name="tel" id="tel" class="form-control" step="1.0" value="<?php echo isset($_SESSION['tel'])? $_SESSION['tel']:'';?>" <?php echo isset($_SESSION['tel']) && !empty($_SESSION['tel'])? 'readonly' :'';?>> 
              </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                  <label for="idproduct">Products</label>
                  <select class="form-control" name="idproduct" id="idproduct" onchange="validProduct(event.target.value)">
                        <option value="" selected>Seleccione...</option>
                      <?php
                          if ($optionsSelect && $sentenciaSelect->rowCount() > 0) {
                              foreach ($optionsSelect as $item) {
                              ?>
                                <option value="<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></option>
                              <?php
                              }
                          }
                      ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="stock">Stock</label>
                  <input type="number" name="stock" id="stock" class="form-control" readonly>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="price">Valor Unidad</label>
                      <input type="number" name="price" id="price" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cant">Cantidad</label>
                      <input type="number" name="cant" id="cant" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="total">Total</label>
                  <input type="number" name="total" id="total" class="form-control" readonly>
                </div>
            </div>
            <div class="col-md-10 mt-4">
            <table class="table" id="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Product</th>
                        <th>Cant</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody id="body-content">
                    <tbody>
                </table>
            </div>
            <div class="col-md-2 mt-4">
                <input type="submit" name="action" class="btn btn-primary" value="add" onclick="add(event)">
                <input type="submit" name="action" class="btn btn-dark" value="Clean All" onclick="remove(event)">
            </div>
        </div>
        <div class="form-group mt-2">
          <input name="csrf" type="hidden" value="<?php echo $_SESSION['csrf']; ?>">
          <input type="submit" name="submit" class="btn btn-success" value="Enviar">
        </div>
      </form>
    </div>
  </div>
</div>

<script>
    let arrayProducts = [];
    let nameP = null;
    let valueP = null;
    let idP = null;

    function validProduct(idproduct) {
        $.ajax({
            method: 'POST',
            data: {id:idproduct},
            datatype: 'json',
            url: '../../../controllers/validProduct.php',
                success: function(data) {
                    let res = JSON.parse(data);
                    if(res.success == 0){
                        const stock = document.getElementById('stock');
                        const price = document.getElementById('price');
                        stock.value = res.data['stock'];
                        price.value = res.data['price'];
                        nameP = res.data['name'];
                        valueP = res.data['price'];
                        idP = res.data['id'];
                    }
                },
                error: function() {
                    alert('There was some error performing the AJAX call!');
                }
            }
        );
    }

    function add(event) {
        event.preventDefault()
        let price = document.getElementById('price').value; 
        let count = document.getElementById('cant').value; 
        if(price == '' || count == ''){
            alert('Debe selecionar un producto y la cantidad');
            return;
        }
        arrayProducts.push({
            name: nameP,
            id: idP,
            cant: parseInt(count),
            value: parseInt(price)*parseInt(count)
        });
        //llenar la tabla
        $("#body-content").append(
            `<tr>
                <td>${idP}</td>
                <td>${nameP}</td>
                <td>${parseInt(count)}</td>
                <td>${parseInt(price)*parseInt(count)}</td>
            </tr>`);
        let total = 0
        arrayProducts.forEach(element => {
            total+=parseInt(element.value)
        });
        document.getElementById('total').value = total;
    }
    function remove(event) {
        event.preventDefault()
        arrayProducts= [];
        //llenar la tabla
        document.location.reload();
        document.getElementById('cant').value = '';
        document.getElementById('total').value = 0;
    }
</script>

<?php include '../../../templates/footer.php'; ?>
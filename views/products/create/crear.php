<?php

include '../../../config/funciones.php';
$config = include '../../../config/config.php';

csrf();
if (isset($_POST['submit']) && !hash_equals($_SESSION['csrf'], $_POST['csrf'])) {
  die();
}
try {
  $dsn = 'mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['name'];
  $conexion = new PDO($dsn, $config['db']['user'], $config['db']['pass'], $config['db']['options']);
  $consultaSQL = "SELECT * FROM category";
  $sentenciaSelect = $conexion->prepare($consultaSQL);
  $sentenciaSelect->execute();
  $optionsSelect = $sentenciaSelect->fetchAll();
} catch(PDOException $error) {
  $resultado['error'] = true;
  $resultado['mensaje'] = $error->getMessage();
}
if (isset($_POST['submit'])) {
  $resultado = [
    'error' => false,
    'mensaje' => 'El product ' . $_POST['name'] . ' ha sido agregado con éxito'
  ];

  try {
    $dsn = 'mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['name'];
    $conexion = new PDO($dsn, $config['db']['user'], $config['db']['pass'], $config['db']['options']);
    $product = [
      "name"   => $_POST['name'],
      "reference" => $_POST['reference'],
      "price"    => $_POST['price'],
      "weight"     => $_POST['weight'],
      "category"    => $_POST['category'],
      "stock"     => $_POST['stock'],
      "state"     => 1,
    ];
    $consultaSQL = "INSERT INTO product (name, reference, price, weight,category,stock,state)";
    $consultaSQL .= "values (:" . implode(", :", array_keys($product)) . ")";
    $sentencia = $conexion->prepare($consultaSQL);
    $sentencia->execute($product);

  } catch(PDOException $error) {
    $resultado['error'] = true;
    $resultado['mensaje'] = $error->getMessage();
  }
}
?>

<?php include '../../../templates/header.php'; ?>

<?php
if (isset($resultado)) {
  ?>
  <div class="container mt-3">
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-<?= $resultado['error'] ? 'danger' : 'success' ?>" role="alert">
          <?= $resultado['mensaje'] ?>
        </div>
      </div>
    </div>
  </div>
  <?php
}
?>

<div class="container box">
  <div class="row">
    <div class="col-md-12">
      <h2 class="mt-4">Create product</h2>
      <form method="post">
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" name="name" id="name" class="form-control">
        </div>
        <div class="form-group">
          <label for="reference">Reference</label>
          <input type="text" name="reference" id="reference" class="form-control">
        </div>
        <div class="form-group">
          <label for="price">Price</label>
          <input type="number" name="price" id="price" class="form-control">
        </div>
        <div class="form-group">
          <label for="weight">Weigth Kg</label>
          <input type="number" name="weight" id="weight" class="form-control"  step="1.0">
        </div>
        <div class="form-group">
          <label for="category">Category</label>
          <select class="form-control" name="category" id="category">
              <?php
                  if ($optionsSelect && $sentenciaSelect->rowCount() > 0) {
                      foreach ($optionsSelect as $item) {
                      ?>
                        <option value="<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></option>
                      <?php
                      }
                  }
              ?>
          </select>
        </div>
        <div class="form-group">
          <label for="stock">Stock</label>
          <input type="number" name="stock" id="stock" class="form-control">
        </div>
        <div class="form-group mt-2">
          <input name="csrf" type="hidden" value="<?php echo $_SESSION['csrf']; ?>">
          <input type="submit" name="submit" class="btn btn-success" value="Enviar">
        </div>
      </form>
    </div>
  </div>
</div>

<?php include '../../../templates/footer.php'; ?>
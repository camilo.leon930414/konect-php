<?php
include '../../../config/funciones.php';
include '../../../config/conexion.php';
csrf();
if (isset($_POST['submit']) && !hash_equals($_SESSION['csrf'], $_POST['csrf'])) {
  die();
}
$error = false;
try {
    $consultaSQL = "SELECT 
    a.created_at 
    ,a.id
    ,a.name
    ,a.price 
    ,a.reference 
    ,c.name as state
    ,a.stock 
    ,a.weight 
    ,b.name as category
    FROM product a JOIN category b ON a.category = b.id 
    JOIN stateproduct c ON a.state = c.id 
    WHERE state != 3";

    $sentencia = $conexion->prepare($consultaSQL);
    $sentencia->execute();

    $products = $sentencia->fetchAll();
} catch(PDOException $error) {
    $error= $error->getMessage();
}

$titulo = isset($_POST['name']) ? 'List the products (' . $_POST['name'] . ')' : 'List the products';
?>
<?php include "../../../templates/header.php"; ?>
<?php
if ($error) {
  ?>
  <div class="container mt-2">
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
          <?= $error ?>
        </div>
      </div>
    </div>
  </div>
  <?php
}
?>
<main class="box mt-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="mt-3"><?= $titulo ?></h2>
            <a href="../create/crear.php"  class="btn btn-primary">Create Product</a>
            <table class="table">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>reference</th>
                    <th>price</th>
                    <th>weight</th>
                    <th>category</th>
                    <th>stock</th>
                    <th>state</th>
                    <th>created_at</th> 
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($products && $sentencia->rowCount() > 0) {
                    foreach ($products as $item) {
                    ?>
                    <tr>
                        <td><?php echo $item["id"]; ?></td>
                        <td><?php echo $item["name"]; ?></td>
                        <td><?php echo $item["reference"]; ?></td>
                        <td><?php echo $item["price"]; ?></td>
                        <td><?php echo $item["weight"]; ?></td>
                        <td><?php echo $item["category"]; ?></td>
                        <td><?php echo $item["stock"]; ?></td>
                        <td><?php echo $item["state"]; ?></td>
                        <td><?php echo $item["created_at"]; ?></td>
                        <td>
                        <a class="btn btn-danger" href="<?= '../delete/borrar.php?id=' . $item["id"] ?>">Borrar</a>
                        <a class="btn btn-warning" href="<?= '../edit/editar.php?id=' . $item["id"] ?>">Editar</a>
                        </td>
                    </tr>
                    <?php
                    }
                }
                ?>
                <tbody>
            </table>
            </div>
        </div>
    </div>
</main>

<?php include "../../../templates/footer.php"; ?>
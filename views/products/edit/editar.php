<?php
include '../../../config/funciones.php';
include '../../../config/conexion.php';

csrf();
if (isset($_POST['submit']) && !hash_equals($_SESSION['csrf'], $_POST['csrf'])) {
  die();
}

try {
  $consultaSQL = "SELECT * FROM category";
  $sentenciaSelect = $conexion->prepare($consultaSQL);
  $sentenciaSelect->execute();
  $optionsSelect = $sentenciaSelect->fetchAll();
} catch(PDOException $error) {
  $resultado['error'] = true;
  $resultado['mensaje'] = $error->getMessage();
}


$resultado = [
  'error' => false,
  'mensaje' => ''
];

if (!isset($_GET['id'])) {
  $resultado['error'] = true;
  $resultado['mensaje'] = 'El product no existe';
}

if (isset($_POST['submit'])) {
  try {
    $dsn = 'mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['name'];
    $conexion = new PDO($dsn, $config['db']['user'], $config['db']['pass'], $config['db']['options']);

    $product = [
      "id"        => $_GET['id'],
      "name"   => $_POST['name'],
      "reference" => $_POST['reference'],
      "price"    => $_POST['price'],
      "weight"     => $_POST['weight'],
      "category"    => $_POST['category'],
      "stock"     => $_POST['stock'],
    ];
    $consultaSQL = "UPDATE product SET
        name ='".$_POST['name']."',
        reference ='".$_POST['reference']."',
        price =".$_POST['price'].",
        weight =".$_POST['weight'].",
        category =".$_POST['category'].",
        stock =".$_POST['stock']."
        WHERE id =".$_GET['id'];

    $consulta = $conexion->prepare($consultaSQL);
    $consulta->execute($product);

  } catch(PDOException $error) {
    $resultado['error'] = true;
    $resultado['mensaje'] = $error->getMessage();
  }
}

try {
  $dsn = 'mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['name'];
  $conexion = new PDO($dsn, $config['db']['user'], $config['db']['pass'], $config['db']['options']);
    
  $id = $_GET['id'];
  $consultaSQL = "SELECT * FROM product WHERE id =" . $id;

  $sentencia = $conexion->prepare($consultaSQL);
  $sentencia->execute();

  $product = $sentencia->fetch(PDO::FETCH_ASSOC);

  if (!$product) {
    $resultado['error'] = true;
    $resultado['mensaje'] = 'No se ha encontrado el product';
  }

} catch(PDOException $error) {
  $resultado['error'] = true;
  $resultado['mensaje'] = $error->getMessage();
}
?>

<?php require "../../../templates/header.php"; ?>

<?php
if ($resultado['error']) {
  ?>
  <div class="container mt-2">
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
          <?= $resultado['mensaje'] ?>
        </div>
      </div>
    </div>
  </div>
  <?php
}
?>

<?php
if (isset($_POST['submit']) && !$resultado['error']) {
  ?>
  <div class="container mt-2">
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-success" role="alert">
          Product updated
        </div>
      </div>
    </div>
  </div>
  <?php
}
?>

<?php
if (isset($product) && $product) {
  ?>
  <div class="container box">
    <div class="row">
      <div class="col-md-12">
        <h2 class="mt-4">Edit Product <?php echo $product['name']; ?></h2>
        <form method="post">
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" name="name" value="<?php echo $product['name']; ?>" id="name" class="form-control">
        </div>
        <div class="form-group">
          <label for="reference">Reference</label>
          <input type="text" name="reference" value="<?php echo $product['reference']; ?>" id="reference" class="form-control">
        </div>
        <div class="form-group">
          <label for="price">Price</label>
          <input type="number" name="price" value="<?php echo $product['price']; ?>" id="price" class="form-control">
        </div>
        <div class="form-group">
          <label for="weight">Weigth Kg</label>
          <input type="number" name="weight" value="<?php echo $product['weight']; ?>" id="weight" class="form-control"  step="1.0">
        </div>
        <div class="form-group">
          <label for="category">Category</label>
          <select class="form-control" name="category" id="category">
              <?php
                  if ($optionsSelect && $sentenciaSelect->rowCount() > 0) {
                      foreach ($optionsSelect as $item) {
                      ?>
                        <option value="<?php echo $item["id"]; ?>" <?php  echo $item["id"] == $product['category'] ? 'Selected':''  ?> ><?php echo $item["name"]; ?></option>
                      <?php
                      }
                  }
              ?>
          </select>
        </div>
        <div class="form-group">
          <label for="stock">Stock</label>
          <input type="number" name="stock" value="<?php echo $product['stock']; ?>" id="stock" class="form-control">
        </div>
        <div class="form-group mt-2">
          <input name="csrf" type="hidden" value="<?php echo $_SESSION['csrf']; ?>">
          <input type="submit" name="submit" class="btn btn-success" value="Enviar">
        </div>
      </form>
      </div>
    </div>
  </div>
  <?php
}
?>

<?php require "../../../templates/footer.php"; ?>
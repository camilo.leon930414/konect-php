<?php
include '../../../config/funciones.php';
include '../../../config/conexion.php';

$resultado = [
  'error' => false,
  'mensaje' => ''
];

try {
    
  $id = $_GET['id'];
  $consultaSQL = "UPDATE product SET state = 3 WHERE id =" . $id;

  $sentencia = $conexion->prepare($consultaSQL);
  $sentencia->execute();

  header('Location: ../../../views/products/list/listproducts.php');

} catch(PDOException $error) {
  $resultado['error'] = true;
  $resultado['mensaje'] = $error->getMessage();
}
?>

<?php require "../../../templates/header.php"; ?>

<div class="container mt-2">
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-danger" role="alert">
        <?= $resultado['mensaje'] ?>
      </div>
    </div>
  </div>
</div>

<?php require "../../../templates/footer.php"; ?>